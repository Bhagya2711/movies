package com.example.movies.exception;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.movies.enums.ErrorCodes;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ErrorCodes.GENERIC_ERROR.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse error = new ErrorResponse(ErrorCodes.VALIDATION_FAILED.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(DuplicateEntryException.class)
	public final ResponseEntity<Object> handleDuplicateEntryException(DuplicateEntryException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ErrorCodes.DUPLICATE_ENTRY.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ValidationException.class)
	public final ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ErrorCodes.VALIDATION_FAILED.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}


	@ExceptionHandler(UserNotFound.class)
	public ResponseEntity<ErrorStatus> handleUserNotFoundException(UserNotFound details) {
		
		ErrorStatus errorStatus = new ErrorStatus();
		errorStatus.setMessage(details.getLocalizedMessage());
		errorStatus.setStatuscode(ErrorCodes.USER_NOT_FOUND.getErrorCode());
		return new ResponseEntity<>(errorStatus, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(TrialExpiredException.class)
	public ResponseEntity<ErrorStatus> handleTrialExpired(TrialExpiredException details) {
		
		   ErrorStatus errorStatus = new ErrorStatus();
	        errorStatus.setMessage(details.getLocalizedMessage());
	        errorStatus.setStatuscode(ErrorCodes.TRIAL_VERSION_END.getErrorCode());
	        return new ResponseEntity<>(errorStatus, HttpStatus.BAD_REQUEST);


	}


}
