/**
 * 
 */
package com.example.movies.exception;

/**
 * @author bhagyashree.naray
 *
 */
public class DuplicateEntryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateEntryException(String message) {
		super(message);
	}

}
