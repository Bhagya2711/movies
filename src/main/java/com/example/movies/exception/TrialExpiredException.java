/**
 * 
 */
package com.example.movies.exception;

/**
 * @author bhagyashree.naray
 *
 */
public class TrialExpiredException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrialExpiredException(String arg0) {
		super(arg0);
	}
	
	

}
