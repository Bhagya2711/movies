package com.example.movies.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.movies.entity.Movie;
import com.example.movies.response.ResponseDto;

@Service
public interface MovieService {
	
	List<Movie> fetchMovies(String genre,String title);
	List<Movie> fetchWatchedMovies(Long userId);
	ResponseDto watchMovie(Long userId,Long movieId);
}
