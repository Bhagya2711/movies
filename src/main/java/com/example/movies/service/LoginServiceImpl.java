package com.example.movies.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.movies.constant.ApplicationConstant;
import com.example.movies.dto.LoginDto;
import com.example.movies.dto.ResponseDto;
import com.example.movies.entity.User;
import com.example.movies.enums.UserType;
import com.example.movies.exception.TrialExpiredException;
import com.example.movies.exception.UserNotFound;
import com.example.movies.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {
	@Autowired
	UserRepository userRepository;

	@Override
	public ResponseDto userLogin(LoginDto loginDto) {
		log.info("UserServiceImpl: registerUser ");
		
		
		
		
	
		Optional<User> findByEmailAndPassword = userRepository.findByEmailAndPassword(loginDto.getEmail(),
				loginDto.getPassword());
		if (!findByEmailAndPassword.isPresent()) {
			log.info( "USER_NOT_FOUND {} ",loginDto.getEmail() );
			throw new UserNotFound(ApplicationConstant.USER_NOT_FOUND);
		}
		User user = findByEmailAndPassword.get();
		if (user.getUserType().equalsIgnoreCase(UserType.TRIAL.getUserType())) {
			if (user.getDate().equals(LocalDate.now())) {
				ResponseDto res=new ResponseDto();
				BeanUtils.copyProperties(user, res);
				return res;
			} else {


				
				throw new TrialExpiredException(ApplicationConstant.TRIAL_VERSION_END);

			}
		}

		ResponseDto response=new ResponseDto();
		BeanUtils.copyProperties(user, response);
		return response;

	}

}
