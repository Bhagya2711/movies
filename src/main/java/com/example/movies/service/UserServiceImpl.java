/**
 * 
 */
package com.example.movies.service;

import java.time.LocalDate;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.movies.entity.User;
import com.example.movies.enums.UserType;
import com.example.movies.exception.DuplicateEntryException;
import com.example.movies.repository.UserRepository;
import com.example.movies.request.AddUserRequest;
import com.example.movies.response.ResponseDto;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public class UserServiceImpl implements UserService {

	/**
	 * 
	 */
	private static final String REGISTERED_SUCCESSFULLY = "Registered Successfully";
	private static final String USER_ALREADY_REGISTERED = "User Already Registered";
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepo;

	@Override
	public ResponseDto registerUser(AddUserRequest request) throws DuplicateEntryException {
		logger.info("UserServiceImpl: registerUser ");

		if (!request.getUserType().equalsIgnoreCase(UserType.PAID.getUserType())
				&& !request.getUserType().equalsIgnoreCase(UserType.TRIAL.getUserType())) {
			throw new ValidationException("Validation Failed- User Type is incorrect");
		}
		User existingUser = userRepo.findByEmail(request.getEmail());
		if (existingUser != null) {
			throw new DuplicateEntryException(USER_ALREADY_REGISTERED);
		}

		User user = new User();
		BeanUtils.copyProperties(request, user);
		user.setDate(LocalDate.now());

		userRepo.save(user);

		return new ResponseDto(REGISTERED_SUCCESSFULLY, 200l);
	}

}
