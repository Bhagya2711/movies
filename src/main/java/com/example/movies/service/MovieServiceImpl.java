/**
 * 
 */
package com.example.movies.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.movies.controller.MovieController;
import com.example.movies.entity.Movie;
import com.example.movies.entity.User;
import com.example.movies.exception.UserNotFound;
import com.example.movies.repository.MovieRepository;
import com.example.movies.repository.UserRepository;
import com.example.movies.response.ResponseDto;


@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	MovieRepository movieRepository;

	@Autowired
	UserRepository userRepository;

	private static final Logger logger = LoggerFactory.getLogger(MovieController.class);
	private static final String USER_NOT_FOUND = "user not found";
	private static final String WATCHED_MOVIE = "watched movie";

	public List<Movie> fetchMovies(String genre, String title) {
		List<Movie> moviesList = new ArrayList<>();
		if (StringUtils.isEmpty(genre) && StringUtils.isEmpty(title)) {
			logger.info("retrieving movie details");
			moviesList = movieRepository.findAll();
		} 
		else{
			if(!StringUtils.isEmpty(genre)){
				moviesList = movieRepository.findByGenreIgnoreCaseContaining(genre);
			}else{
				moviesList = movieRepository.findByTitleIgnoreCaseContaining(title);
			}
		
		}
		
		return moviesList;
	}

	public List<Movie> fetchWatchedMovies(Long userId) {
		List<Long> movieIdList = movieRepository.findWatchedMoviesById(userId);
		List<Movie> movieList = new ArrayList<>();
		movieIdList.forEach(temp -> {
			Optional<Movie> optMovie = movieRepository.findById(temp);
			if (optMovie.isPresent()) {
				movieList.add(optMovie.get());
			}

		});
		return movieList;
	}

	public ResponseDto watchMovie(Long userId, Long movieId) {
		Optional<User> optionalUser = userRepository.findById(userId);
		Optional<Movie> optionalMovie = movieRepository.findById(movieId);

		if (!optionalUser.isPresent()) {
			throw new UserNotFound(USER_NOT_FOUND);
		}
		List<Movie> movieList = optionalUser.get().getMovies();
		if (optionalMovie.isPresent()&& !movieList.contains(optionalMovie.get()) ) {
				movieList.add(optionalMovie.get());

		}
		optionalUser.get().setMovies(movieList);
		userRepository.save(optionalUser.get());

		return new ResponseDto(WATCHED_MOVIE, 200l);
	}

}
