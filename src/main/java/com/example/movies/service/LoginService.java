/**
 * 
 */
package com.example.movies.service;

import org.springframework.stereotype.Service;

import com.example.movies.dto.LoginDto;
import com.example.movies.dto.ResponseDto;

/**
 * @author rakeshr.ra
 *
 */
@Service
public interface LoginService {
	
	
	public ResponseDto userLogin( LoginDto loginDto);
}
