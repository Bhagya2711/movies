/**
 * 
 */
package com.example.movies.service;

import org.springframework.stereotype.Service;

import com.example.movies.exception.DuplicateEntryException;
import com.example.movies.request.AddUserRequest;
import com.example.movies.response.ResponseDto;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public interface UserService {

	/**
	 * @param request
	 * @return
	 * @throws DuplicateEntryException 
	 */
	ResponseDto registerUser( AddUserRequest request) throws DuplicateEntryException;
	
	

}
