
package com.example.movies.repository;

import java.util.Optional;

/**
 * 
 */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.movies.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByEmailAndPassword(String email, String password);

	/**
	 * @param email
	 */
	User findByEmail(String email);

}
