package com.example.movies.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.movies.entity.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

	List<Movie> findByGenreIgnoreCaseContaining(String genre);

	List<Movie> findByTitleIgnoreCaseContaining(String title);

	@Query(value = "select movie_id from user_movie where user_id=?1", nativeQuery = true)
	List<Long> findWatchedMoviesById(Long userId);

}
