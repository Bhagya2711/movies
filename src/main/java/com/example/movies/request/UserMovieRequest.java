/**
 * 
 */
package com.example.movies.request;

import lombok.Data;

/**
 * @author hemanth.garlapati
 * 
 */
@Data
public class UserMovieRequest {

	private Long userId;
	private Long movieId;

}
