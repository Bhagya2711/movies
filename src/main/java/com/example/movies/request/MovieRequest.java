/**
 * 
 */
package com.example.movies.request;

import lombok.Data;

/**
 * @author hemanth.garlapati
 * 
 */
@Data
public class MovieRequest {

	private String genre;
	private String title;

}
