/**
 * 
 */
package com.example.movies.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.example.movies.annotation.ValidPassword;

import lombok.Data;

/**
 * @author bhagyashree.naray
 *
 */
@Data
public class AddUserRequest {
	
	@NotNull(message= "User name must not be null")
	private String userName;
	
	private String userType;
	
	@ValidPassword
	@NotNull(message= "Password must not be null")
	private String password;
	
	@Email(message="Invalid email id, try again")
	@NotNull(message= "User Email Id must not be null")
	private String email;
	
	

}
