/**
 * 
 */
package com.example.movies.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.movies.exception.DuplicateEntryException;
import com.example.movies.request.AddUserRequest;
import com.example.movies.response.ResponseDto;
import com.example.movies.service.UserService;

import io.swagger.annotations.ApiOperation;

/**
 * @author bhagyashree.naray
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	UserService userService;

	/**
	 * Add a user @param request @return ResponseDto @throws
	 */
	@ApiOperation(value = "Register a user")
	@PostMapping("/users")
	public ResponseEntity<ResponseDto> registerUser(@Valid @RequestBody AddUserRequest request)
			throws DuplicateEntryException {
		logger.info("UserController: registerUser ");
		return new ResponseEntity<>(userService.registerUser(request), HttpStatus.OK);

	}

}
