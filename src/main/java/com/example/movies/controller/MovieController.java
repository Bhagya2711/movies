package com.example.movies.controller;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.movies.entity.Movie;
import com.example.movies.repository.MovieRepository;
import com.example.movies.request.UserMovieRequest;
import com.example.movies.response.ResponseDto;
import com.example.movies.service.MovieService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MovieController {
	
	@Autowired
	MovieService movieService;
	
	@Autowired
	MovieRepository movieRepository;
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MovieController.class);

	/**
	 * 
	 * @param request
	 *
	 * @return list of Movies
	 * 
	 */
	@ApiOperation(value = "fetch movies")
	@GetMapping(value = "movies")
	public ResponseEntity<List<Movie>> getMovies( @RequestParam(name= "genre",required=false)  String genre,
			@RequestParam(name= "title",required=false)  String title) {
		logger.info(" Fetching movies details");
		List<Movie> movieList=movieService.fetchMovies(genre,title);
		return new ResponseEntity<>(movieList, HttpStatus.OK);

	}
	
	/**s
	 * 
	 * @param userId
	 *
	 * @return list of watched Movies
	 * @author hemanth.garlapati
	 */
	@ApiOperation(value = "fetching watched movies")
	@GetMapping(value = "/users/{userId}/movies")
	public ResponseEntity<List<Movie>> getWatchedMoviesList(@PathVariable Long userId) {
		logger.info(" Fetching watched movies details");
		return new ResponseEntity<>(movieService.fetchWatchedMovies(userId), HttpStatus.OK);

	}
	
	/**
	 * 
	 * @param request
	 *
	 * @return list of watched Movies
	 * @author hemanth.garlapati
	 */
	@ApiOperation(value = "Watching a movie")
	@PostMapping(value = "/user-movies")
	public ResponseEntity<ResponseDto> watchMovie(@RequestBody UserMovieRequest request) {
		logger.info(" watching a movie");
		return new ResponseEntity<>(movieService.watchMovie(request.getUserId(),request.getMovieId()), HttpStatus.OK);

	}
}
