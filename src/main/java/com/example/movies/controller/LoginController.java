/**
 * 
 */
package com.example.movies.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.movies.dto.LoginDto;
import com.example.movies.dto.ResponseDto;
import com.example.movies.service.LoginService;

import lombok.extern.slf4j.Slf4j;

/**
 * @param loginDto
 * @return responseDto
 * @author rakeshr.ra
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Slf4j
public class LoginController {

	@Autowired
	LoginService loginService;

	@PostMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@RequestBody LoginDto loginDto) {
		log.info("LoginController: userLogin ");
		return new ResponseEntity<>(loginService.userLogin(loginDto), HttpStatus.OK);
	}

}
