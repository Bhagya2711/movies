/**
 * 
 */
package com.example.movies.enums;

/**
 * @author rakeshr.ra
 *
 */
public enum UserType {
	TRIAL("Trial"), PAID("Paid");

	private String userTyp;

	/**
	 * @param userType
	 */
	private UserType(String userType) {
		this.userTyp = userType;
	}

	public String getUserType() {
		return userTyp;
	}

}
