/**
 * 
 */
package com.example.movies.enums;

/**
 * @author hemanth.garlapati
 *
 */
public enum ErrorCodes {

	

	USER_NOT_FOUND(4041l), VALIDATION_FAILED(4001l), GENERIC_ERROR(5001l), DUPLICATE_ENTRY(4002l),TRIAL_VERSION_END(4022l),;


	Long errorCode;

	ErrorCodes(Long value) {
		this.errorCode = value;
	}

	public Long getErrorCode() {
		return errorCode;
	}

}
