/**
 * 
 */
package com.example.movies.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.movies.exception.DuplicateEntryException;
import com.example.movies.request.AddUserRequest;
import com.example.movies.response.ResponseDto;
import com.example.movies.service.UserServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class UserControllerTest {
	
	@Mock
	UserServiceImpl userServiceImpl;

	@InjectMocks
	UserController userController;
	
	@BeforeEach
	public void init() {

	}

	@Test
	public void testRegisterUser() throws DuplicateEntryException{
		
		AddUserRequest request=new AddUserRequest();
		request.setEmail("abc@email.com");
		request.setPassword("Abc@1");
		request.setUserName("abc");
		request.setUserType("trial");
		
		ResponseDto dto=new ResponseDto("success", 200L);
		when(userServiceImpl.registerUser(request)).thenReturn(dto);
		assertEquals(200,userController.registerUser(request).getStatusCodeValue());
				
		
	}

}
