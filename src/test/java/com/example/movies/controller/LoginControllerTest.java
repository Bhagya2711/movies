package com.example.movies.controller;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.movies.constant.ApplicationConstant;
import com.example.movies.dto.LoginDto;
import com.example.movies.dto.ResponseDto;
import com.example.movies.service.LoginService;

@SpringBootTest
public class LoginControllerTest {
	@InjectMocks
	LoginController loginController;
	@Mock
	LoginService loginService;
	LoginDto loginDto = new LoginDto("way2rakesh", "12345");
	ResponseDto responseDto = new ResponseDto();

	@BeforeEach
	public void init() {
		responseDto.setEmail("way2rakeshr@gmail.com");
		responseDto.setUserName("rakesh");
	}

	@Test
	public void userLogin() {
		Mockito.when(loginService.userLogin(loginDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> userLogin = loginController.userLogin(loginDto);
		Assertions.assertEquals(userLogin.getStatusCodeValue(), HttpStatus.OK.value());
	}
}
