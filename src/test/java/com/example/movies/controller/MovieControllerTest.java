/**
 * 
 */
package com.example.movies.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.movies.entity.Movie;
import com.example.movies.entity.User;
import com.example.movies.request.MovieRequest;
import com.example.movies.service.MovieService;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class MovieControllerTest {

	@Mock
	MovieService movieServiceImpl;

	@InjectMocks
	MovieController movieController;

	User user = new User();

	@BeforeEach
	public void init() {

	}

	@Test
	public void testFetchMovies() {
		Movie movie1 = new Movie(1L, "bahubali", "action", "nice");
		Movie movie2 = new Movie(2L, "dangal", "patriotic", "excellent");
		List<Movie> movieList = new ArrayList<>();
		movieList.add(movie1);
		movieList.add(movie2);
		
		MovieRequest request=new MovieRequest();
		request.setGenre("");
		request.setTitle("");
		when(movieServiceImpl.fetchMovies("", "")).thenReturn(movieList);
		assertEquals(200, movieController.getMovies("Comedy","").getStatusCodeValue());

	}

	@Test
	public void testFetchWatchedMovies() {
		Movie movie1 = new Movie(1L, "bahubali", "action", "nice");
		Movie movie2 = new Movie(2L, "dangal", "patriotic", "excellent");
		List<Movie> movieList = new ArrayList<>();
		movieList.add(movie1);
		movieList.add(movie2);

		when(movieServiceImpl.fetchWatchedMovies(1L)).thenReturn(movieList);
		assertEquals(200, movieController.getWatchedMoviesList(1L).getStatusCodeValue());

	}
}
