/**
* 
 */
package com.example.movies.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.movies.entity.User;
import com.example.movies.exception.DuplicateEntryException;
import com.example.movies.repository.UserRepository;
import com.example.movies.request.AddUserRequest;
import com.example.movies.response.ResponseDto;

/**
 * @author bhagyashree.naray
 *
 */
@SpringBootTest
public class UserServiceImplTest {

	@Mock
	UserRepository userRepo;

	@InjectMocks
	UserServiceImpl userServieImpl;

	


	/*
	 * @Test public void testRegister() {
	 * 
	 * AddUserRequest request = new AddUserRequest(); request.setEmail("a@gmail");
	 * request.setPassword("Abc123@");
	 * 
	 * User user = new User(); user.setUserId(2l); user.setPassword("abc123A@");
	 * 
	 * when(userRepo.findByEmail(request.getEmail())).thenReturn(user);
	 * 
	 * assertThrows(DuplicateEntryException.class, () ->
	 * userServieImpl.registerUser(request));
	 * 
	 * }
	 */
	/*
	 * @Test public void testRegisterSuccess() throws DuplicateEntryException {
	 * 
	 * AddUserRequest request = new AddUserRequest(); request.setEmail("xyz@gmail");
	 * request.setPassword("Abc123@");
	 * 
	 * User user = new User(); user.setUserId(2l); user.setPassword("abc123A@");
	 * 
	 * when(userRepo.findByEmail(request.getEmail())).thenReturn(null);
	 * 
	 * ResponseDto dto = userServieImpl.registerUser(request);
	 * assertEquals("Registered Successfully", dto.getMessage());
	 * 
	 * }
	 */

	@Test
	public void testRegister() {

		AddUserRequest request = new AddUserRequest();
		request.setEmail("a@gmail");
		request.setPassword("Abc123@");
		request.setUserType("Trial");
		request.setUserName("Test");

		User user = new User();
		user.setUserId(2l);
		user.setPassword("abc123A@");

		when(userRepo.findByEmail(request.getEmail())).thenReturn(user);

		assertThrows(DuplicateEntryException.class, () -> userServieImpl.registerUser(request));

	}

	@Test
	public void testRegisterSuccess() throws DuplicateEntryException {

		AddUserRequest request = new AddUserRequest();
		request.setEmail("xyz@gmail");
		request.setPassword("Abc123@");
		request.setUserType("Trial");
		request.setUserName("Test");

		User user = new User();
		user.setUserId(2l);
		user.setPassword("abc123A@");

		when(userRepo.findByEmail(request.getEmail())).thenReturn(null);

		ResponseDto dto = userServieImpl.registerUser(request);
		assertEquals("Registered Successfully", dto.getMessage());

	}


}
