/**
 * 
 */
package com.example.movies.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.movies.entity.Movie;
import com.example.movies.entity.User;
import com.example.movies.repository.MovieRepository;
import com.example.movies.repository.UserRepository;

@SpringBootTest
public class MovieServiceTest {

	@InjectMocks
	MovieServiceImpl movieServiceImpl;

	@Mock
	MovieRepository movieRepository;

	@Mock
	UserRepository userRepo;
	List<Movie> movieList;

	@BeforeEach
	public void init() {

		Movie movie1 = new Movie(1L, "bahubali", "action", "nice");
		Movie movie2 = new Movie(1L, "kgf", "Thriller", "nice");
		movieList = new ArrayList<>();
		movieList.add(movie1);
		movieList.add(movie2);

	}

	@Test
	public void testFetchMovies() {
		Movie movie1 = new Movie(1L, "bahubali", "action", "nice");
		Movie movie2 = new Movie(2L, "dangal", "patriotic", "excellent");
		List<Movie> movieList = new ArrayList<>();
		movieList.add(movie1);
		movieList.add(movie2);
		when(movieRepository.findAll()).thenReturn(movieList);
		// when(movieServiceImpl.fetchMovies(Mockito.eq(""),
		// Mockito.eq(""))).thenReturn(movieList);
		assertEquals(2, movieServiceImpl.fetchMovies("", "").size());

	}

	/*
	 * @Test public void testFetchMoviesForNull() { Movie movie1=new Movie(1L,
	 * "bahubali", "action", "nice"); Movie movie2=new Movie(2L, "dangal",
	 * "patriotic", "excellent"); List<Movie> movieList=new ArrayList<>();
	 * List<Movie> movieList1=new ArrayList<>(); movieList.add(movie1);
	 * movieList.add(movie2); when(movieRepository.findAll()).thenReturn(movieList);
	 * assertEquals(2, movieServiceImpl.fetchMovies(null, null).size());
	 * 
	 * }
	 */
	@Test
	public void testFetchSearcgOnTitle() {

		Mockito.when(movieRepository.findByTitleIgnoreCaseContaining("kgf")).thenReturn(movieList);
		List<Movie> fetchMovies = movieServiceImpl.fetchMovies("", "kgf");
		Assertions.assertEquals(2, fetchMovies.size());
        
	}
	
	@Test
	public void testFetchSearcgOngenere() {
		Movie movie1 = new Movie(1L, "bahubali", "action", "nice");
		Movie movie2 = new Movie(1L, "kgf", "Thriller", "nice");
		ArrayList<Movie> movies=new ArrayList<Movie>();
		movies.add(movie1);
		movies.add(movie2);
		String genere="drama";
		String tilte=null;
		Mockito.when(movieRepository.findByGenreIgnoreCaseContaining(genere)).thenReturn(movieList);
		List<Movie> fetchMovies = movieServiceImpl.fetchMovies(tilte, genere);
		Assertions.assertEquals(fetchMovies.size(),0);
        
	}
	

	@Test
	public void testFetchwatchedMovies() {
		Movie movie1 = new Movie(1L, "bahubali", "action", "nice");

		List<Long> ll = new ArrayList<>();
		ll.add(1L);
		ll.add(2L);
		when(movieRepository.findWatchedMoviesById(1L)).thenReturn(ll);
		when(movieRepository.findById(1L)).thenReturn(Optional.of(movie1));
		assertEquals(1, movieServiceImpl.fetchWatchedMovies(1L).size());

	}

	@Test
	public void testWatchMovies() {
		Movie movie1 = new Movie(1L, "bahubali", "action", "nice");

		User user = new User();
		user.setEmail("test@gmail.com");
		user.setUserId(2l);
		user.setPassword("abc123A@");
		Movie movie2 = new Movie(2L, "dangal", "patriotic", "excellent");
		List<Movie> movieList = new ArrayList<>();
		movieList.add(movie1);
		movieList.add(movie2);
		user.setMovies(movieList);

		when(movieRepository.findById(1L)).thenReturn(Optional.of(movie1));
		when(userRepo.findById(1l)).thenReturn(Optional.of(user));
		assertEquals("watched movie", movieServiceImpl.watchMovie(1l, 1l).getMessage());

	}

}
