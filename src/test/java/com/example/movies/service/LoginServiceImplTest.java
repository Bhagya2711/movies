package com.example.movies.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.movies.constant.ApplicationConstant;
import com.example.movies.dto.LoginDto;
import com.example.movies.dto.ResponseDto;
import com.example.movies.entity.User;
import com.example.movies.exception.TrialExpiredException;
import com.example.movies.exception.UserNotFound;
import com.example.movies.repository.UserRepository;





@SpringBootTest
public class LoginServiceImplTest {


	@Mock
	UserRepository userRepository;
	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	LoginDto loginDto;
	User user;
	User user2;
	User user3;
	User user4;
	List<User> list=new ArrayList<>();

	@BeforeEach
	public void init() {
		user4 = new User();
		user = new User(1L, "rakesh", "trial", "way2rakesh", "12345", LocalDate.now(), null);
		user2 = new User(2L, "hemanth", "paid", "hemanth", "12345", LocalDate.now().minusDays(3L), null);
		user3 = new User(3L, "bhagya", "trial", "bhagvya@gmail.com", "12345", LocalDate.now().minusDays(3L), null);
		list = Stream.of(new User(1L, "rakesh", "trial", "way2rakesh", "12345", LocalDate.now(), null),
				new User(2L, "hemanth", "paid", "hemanth", "12345", LocalDate.now().minusDays(3L), null),
				new User(3L, "bhagya", "trial", "bhagvya@gmail.com", "12345", LocalDate.now().minusDays(3L), null))
				.collect(Collectors.toList());
		loginDto = new LoginDto("way2rakesh", "12345");

	}

	@Test
	public void userLogin() {
		Mockito.when(userRepository.findByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword()))
				.thenReturn(Optional.of(user));
		ResponseDto userLogin = loginServiceImpl.userLogin(loginDto);
		Assertions.assertEquals(userLogin.getUserName(), "rakesh");

	}

	@Test
	public void userLoginNegative() {
		try {
			loginDto = new LoginDto("way2rakesh", "123");
			Mockito.when(userRepository.findByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword()))
					.thenReturn(Optional.of(user));
			ResponseDto userLogin = loginServiceImpl.userLogin(loginDto);
		} catch (UserNotFound user) {
			Assertions.assertEquals(user.getLocalizedMessage(), "user not found");
		}

	}

	@Test
	public void userLoginPaid() {
		loginDto = new LoginDto("hemanth", "12345");
		Mockito.when(userRepository.findByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword()))
				.thenReturn(Optional.of(user2));
		ResponseDto userLogin = loginServiceImpl.userLogin(loginDto);
		Assertions.assertEquals(userLogin.getUserName(), "hemanth");

	}


 

    @Test
    public void userLoginNegativeTrial() {
        try {
            loginDto = new LoginDto("bhagvya@gmail.com", "12345");
            Mockito.when(userRepository.findByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword()))
                    .thenReturn(Optional.of(user3));
            ResponseDto userLogin = loginServiceImpl.userLogin(loginDto);

      
        } catch (TrialExpiredException user) {

            Assertions.assertEquals(user.getLocalizedMessage(), ApplicationConstant.TRIAL_VERSION_END);
        }

 

    }


}
 
















